
# from reportlab.pdfgen import canvas
# from reportlab.lib import colors
# from reportlab.lib.pagesizes import letter, inch
# from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Spacer
# import babel.numbers as bn
# from  reportlab.lib.styles import ParagraphStyle as PS
# from  reportlab.platypus import PageBreak, Flowable
# from  reportlab.platypus.paragraph import Paragraph
# import reportlab.platypus as pltpus
# import jinja2
# from wand.image import Image
#from wand.display import display
# from wand.image import Color
from utils import *
import yaml
import hashlib as hl
import re




def payment_information(elements):

    """
    Generate a payment information set of tables and append to the input elements

    :param elements: elements to add tables to

    :type elements: list of elements
    :raises: None
    :return: None
    """

    elements.append(Spacer(0, 0.3 * inch))
    elements.append(Paragraph('US Domestic Transfers:', PS("body")))
    elements.append(Spacer(0, 0.2 * inch))

    # Data for first table (Domestic transfers)
    data = [[Paragraph('Credit account (Account Name)', PS("body")), Paragraph('TRANSIGO INC', PS('body'))],
            [Paragraph('Address ', PS("body")), Paragraph('11 SOUTH 12TH STREET, RICHMOND VA 23219', PS("body"))],
            [Paragraph('Credit account number', PS("body")), Paragraph('3302503851', PS("body"))],
            [Paragraph('Bank name and address', PS("body")),
             Paragraph('SIL VLY BK SJ, 3003 TASMAN DRIVE, SANTA CLARA, CA, 95054', PS("body"))],
            [Paragraph('Bank routing & transit Number ', PS("body")), Paragraph('121140399', PS("body"))]]

    height = wrap_text_and_compute_height(data, table_width=2 * inch)

    t = Table(data, 2 * [2 * inch], height)

    t.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))

    elements.append(t)

    elements.append(Spacer(0, 0.3 * inch))

    elements.append(Paragraph('International Transfers:', PS('body')))
    elements.append(Spacer(0, 0.2 * inch))

    # Data for second table (International Transfers)
    data = [[Paragraph('Credit account (Account Name)', PS("body")), Paragraph('TRANSIGO INC', PS('body'))],
            [Paragraph('Address ', PS("body")), Paragraph('11 SOUTH 12TH STREET, RICHMOND VA 23219', PS("body"))],
            [Paragraph('Credit account number', PS("body")), Paragraph('3302503851', PS("body"))],
            [Paragraph('Bank name and address', PS("body")),
             Paragraph('SILICON VALLEY BANK, 3003 TASMAN DRIVE, SANTA CLARA, CA, 95054, USA', PS("body"))],
            [Paragraph('Bank SWIFT code', PS('body')), Paragraph('SVBKUS6S', PS('body'))],
            [Paragraph('Bank routing & transit Number ', PS("body")), Paragraph('121140399', PS("body"))]]

    height = wrap_text_and_compute_height(data, table_width=2 * inch)

    t = Table(data, 2 * [2 * inch], height)

    t.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black)]))

    elements.append(t)


def generate_importer_acceptance_agreement(fname, importer_p, exporter_p, transigo_p, deal_p, po_pi_p, document_signature_date, tags = True):

    """


    #TODO rewrite docstring

    Generate complete acceptance agreement in pdf format including all schedules and signature page



    """


    # Money formatting class

    PM = FormatCurrency(currency=deal_p["currency"], locale=deal_p["locale"])

    # read and format image to a flowable

    image_flowable = read_pdf_convert_rescale(fname=po_pi_p["proforma_fname"])

    #Prepare agreement text

    agreement_paragraphs,ver = file_to_Text("importer_agreement.txt",transigo_address=transigo_p["address"], importer_address=importer_p["address"],
                                importer_name=importer_p["name"], exporter_name=exporter_p["name"], exporter_address = exporter_p["address"],
                                per_inv_date = po_pi_p["proforma_invoice_date"], res_due = PM.PrintM(deal_p["reserve"]), importer_email=importer_p["email"], transigo_email=transigo_p["support_email"])


    doc = SimpleDocTemplate(fname, pagesize=letter)

    # container for the 'Flowable' objects
    elements = []

    # Generate payment table

    data = [['Installment', 'Due Date', 'Amount Due - $'],
            ['Down payment', po_pi_p["proforma_invoice_date"], PM.PrintM(deal_p["downpayment"])],
            #['Reserve', res_date, PM.PrintM(res_due)],
            ['Second installment', po_pi_p["est_second_installment_date"], PM.PrintM(deal_p["second_installment"])],
            ['Total', '', PM.PrintM(deal_p["downpayment"] +deal_p["second_installment"])]] # +res_due

    t = Table(data, 3 * [2 * inch], 4 * [0.4 * inch])

    t.setStyle(TableStyle([('LINEBELOW', (0,0), (-1,0), 1, colors.black), #bold line after first row
                           ('LINEBELOW', (0,3), (-1,3), 1, colors.black), #bold line after fourth row
                           ('LINEAFTER', (0,0) , (0,-1), 1, colors.black), #bold line after first column
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BACKGROUND', (0,0), (-1,0), colors.lightgrey),
                           ('BACKGROUND', (0,0), (0,-1), colors.lightgrey),
                           ('BACKGROUND', (2, 4), (2, 4), colors.lightgrey)
                           ]))

    centered = PS(name='centered',
                  fontSize=12,
                  leading=16,
                  alignment=1,
                  spaceAfter=10)


    #generate entire text

    for x in agreement_paragraphs:
        elements.append(Paragraph(x,PS("Justify")))

    elements.append(PageBreak())
    # Generate signers page
    signers = []
    signers.append({'name': transigo_p["contact"], 'company': transigo_p["name"], 'email_address': transigo_p["contact_email"]})
    signers.append({'name': importer_p["contact"], 'company': importer_p["name"], 'email_address': importer_p["address"]})
    SignerPage = SignaturePage (signers=signers, tags=tags, date = document_signature_date)
    elements.append(SignerPage)
    elements.append(PageBreak())

    # Generate Schedule A

    elements.append(Paragraph('<b><u>SCHEDULE A</u></b>',centered))
    elements.append(Paragraph('<u>Purchase Order</u>', centered))
    elements.append(Spacer(0,0.2*inch))
    elements.append(image_flowable) # Append image to elements
    elements.append(PageBreak())

    #Generate Schedule B

    elements.append(Paragraph('<b><u>SCHEDULE B</u></b>',centered))
    elements.append(Paragraph('<u>Payment Terms</u>', centered))

    elements.append(t) # append the tabled defined before
    elements.append(Spacer(0,0.2*inch))
    elements.append(Paragraph("* A Reserve deposit of %s is due when invoice is issued, currently estimated on %s" % ( PM.PrintM(deal_p["reserve"]), deal_p["est_invoice_date"]),PS("Justify")))

    elements.append(PageBreak())

    elements.append(Paragraph('<b><u>SCHEDULE C</u></b>',centered))
    elements.append(Paragraph('<b><u>Wire Transfer Instructions for Reserve Account</u></b>',centered))
    payment_information(elements) # Append the predefined payment information tables

    # write the document to disk

    build_document(doc=doc, elements=elements, version=ver)

    # ----------------------------------------------------------------------



def generate_exporter_schedA(fname, exporter_p, importer_p, transigo_p, deal_p, po_pi_p, document_signature_date , tags=True):


    """ Generate Sched A or Appendix C of exporter agreement.

    :param fname: (pdf) Output file name
    :param type: Either "SchedA" or "AppendixC" for document type
    :param exporter_p: Exporter dictionary
    :param importer_p: Importer dictionary
    :param transigo_p: Transigo dictionary
    :param deal_p:     Deal dictionary
    :param tags:       Whether to include Hellosign tags

    :type fname: str
    :type type: str
    :type exporter_p: dict
    :type importer_p: dict
    :type transigo_p: dict
    :type deal_p: dict
    :type tags: bool


    :return: None

    """

    doc = SimpleDocTemplate(fname, pagesize=letter)
    # container for the 'Flowable' objects
    elements = []


    paragraphs, ver = file_to_Text("SchedA.txt",
                                    contract_date=exporter_p["MSA_date"], exporter_name=exporter_p["name"],
                                    exporter_address=exporter_p["address"], transigo_address=transigo_p["address"])

    PM = FormatCurrency(currency= deal_p["currency"], locale= deal_p["locale"])


    data = [[Paragraph("Commercial Agreement / PO / Proforma invoice number",PS("Normal")), Paragraph(po_pi_p["proforma_invoice_num"],PS("Normal"))],
            [Paragraph("Debtor name & address",PS("Normal")), Paragraph(importer_p["name"] + "<br/>" + importer_p["address"],PS("Normal"))],
            [Paragraph("Amount due in first installment",PS("Normal")), Paragraph(PM.PrintM(deal_p["downpayment"]),PS("Normal"))],
            [Paragraph("Amount due in second installment",PS("Normal")), Paragraph(PM.PrintM(deal_p["second_installment"]),PS("Normal"))],
            [Paragraph("Estimated invoicing date (date when shipment is ready to be shipped)", PS("Normal")), Paragraph(deal_p["est_invoice_date"],PS("Normal"))],
            [Paragraph("Second installment payment Term",PS("Normal")), Paragraph("%d Days" % (deal_p["credit_invoice_term"]),PS("Normal"))],
            [Paragraph("Estimated Second installment due date (Estimated invoicing date + Invoice term)",PS("Normal")), Paragraph(po_pi_p["est_second_installment_date"],PS("Normal"))],
            [Paragraph("Factoring fee",PS("Normal")), Paragraph(PM.PrintM(deal_p["factoring_fee"]),PS("Normal"))],
            [Paragraph("Purchase price(Amount due in second installment – Factoring fee + exporter commission)",PS("Normal")), Paragraph(PM.PrintM(deal_p["purchase_price"]),PS("Normal"))]
            ]
#TODO: Check with Omer whether these lines are required

    height = wrap_text_and_compute_height(data,table_width=3*inch)

    t = Table(data, 2 * [3 * inch], height)

    t.setStyle(TableStyle([('LINEBELOW', (0, 0), (-1, 0), 1, colors.black),  # bold line after first row
                           ('LINEBELOW', (0, 3), (-1, 3), 1, colors.black),  # bold line after fourth row
                           ('LINEAFTER', (0, 0), (0, -1), 1, colors.black),  # bold line after first column
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
                           ('BACKGROUND', (0, 0), (0, -1), colors.lightgrey),
                           ('BACKGROUND', (2, 4), (2, 4), colors.lightgrey)
                           ]))

    width, height = letter

    w, h = t.wrap(width, height)

    for x in paragraphs:
        if x=="//T1":
            elements.append(t) # append table
        else:
            elements.append(Paragraph(x,PS("Justify")))

    elements.append(PageBreak())
    signers = []

    signers.append({'name': transigo_p["contact"], 'company': transigo_p["name"], 'email_address': transigo_p["contact_email"]})
    signers.append({'name': exporter_p["contact"], 'company': exporter_p["name"], 'email_address': exporter_p["email"]})


    signer_page = SignaturePage(signers=signers, tags=tags, date=document_signature_date)
    elements.append(signer_page)
    elements.append(PageBreak())

    build_document(doc=doc, elements=elements, version=ver)

    # ----------------------------------------------------------------------


def generate_exporter_appendixC(fname, exporter_p, importer_p, transigo_p, deal_p, invoice_p, document_signature_date, tags=True):


    """ Generate Sched A or Appendix C of exporter agreement.

    :param fname: (pdf) Output file name
    :param type: Either "SchedA" or "AppendixC" for document type
    :param exporter_p: Exporter dictionary
    :param importer_p: Importer dictionary
    :param transigo_p: Transigo dictionary
    :param deal_p:     Deal dictionary
    :param tags:       Whether to include Hellosign tags

    :type fname: str
    :type type: str
    :type exporter_p: dict
    :type importer_p: dict
    :type transigo_p: dict
    :type deal_p: dict
    :type tags: bool


    :return: None

    """

    doc = SimpleDocTemplate(fname, pagesize=letter)
    # container for the 'Flowable' objects
    elements = []

    paragraphs, ver = file_to_Text("appendixC.txt",
                                    contract_date=exporter_p["MSA_date"], exporter_name=exporter_p["name"],
                                    exporter_address=exporter_p["address"], transigo_address=transigo_p["address"])

    PM = FormatCurrency(currency= deal_p["currency"], locale= deal_p["locale"])

    data = [[Paragraph("Invoice Number",PS("Normal")), Paragraph(invoice_p["invoice_num"],PS("Normal"))],
            [Paragraph("Debtor name & address",PS("Normal")), Paragraph(importer_p["name"] + "<br/>" + importer_p["address"],PS("Normal"))],
            [Paragraph("Amount due in first installment",PS("Normal")), Paragraph(PM.PrintM(deal_p["downpayment"]),PS("Normal"))],
            [Paragraph("Amount due in second installment",PS("Normal")), Paragraph(PM.PrintM(deal_p["second_installment"]),PS("Normal"))],
            [Paragraph("Invoice date", PS("Normal")), Paragraph(invoice_p["invoice_date"],PS("Normal"))],
            [Paragraph("Invoice Term",PS("Normal")), Paragraph("%d Days" % (deal_p["credit_invoice_term"]),PS("Normal"))],
            [Paragraph("Second installment due date ",PS("Normal")), Paragraph(invoice_p["second_installment_date"],PS("Normal"))],
            [Paragraph("Factoring fee",PS("Normal")), Paragraph(PM.PrintM(deal_p["factoring_fee"]),PS("Normal"))],
            [Paragraph("Purchase price(Amount due in second installment – Factoring fee + importer commission)",PS("Normal")), Paragraph(PM.PrintM(deal_p["purchase_price"]),PS("Normal"))]
            ]

    height = wrap_text_and_compute_height(data,table_width=3*inch)

    t = Table(data, 2 * [3 * inch], height)

    t.setStyle(TableStyle([('LINEBELOW', (0, 0), (-1, 0), 1, colors.black),  # bold line after first row
                           ('LINEBELOW', (0, 3), (-1, 3), 1, colors.black),  # bold line after fourth row
                           ('LINEAFTER', (0, 0), (0, -1), 1, colors.black),  # bold line after first column
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
                           ('BACKGROUND', (0, 0), (0, -1), colors.lightgrey),
                           ('BACKGROUND', (2, 4), (2, 4), colors.lightgrey)
                           ]))

    width, height = letter

    w, h = t.wrap(width, height)

    for x in paragraphs:
        if x=="//T1":
            elements.append(t) # append table
        else:
            elements.append(Paragraph(x,PS("Justify")))

    elements.append(PageBreak())
    signers = []

    signers.append({'name': transigo_p["contact"], 'company': transigo_p["name"], 'email_address': transigo_p["contact_email"]})
    signers.append({'name': exporter_p["contact"], 'company': exporter_p["name"], 'email_address': exporter_p["email"]})

    signer_page = SignaturePage(signers=signers, tags=tags, date=document_signature_date)
    elements.append(signer_page)
    elements.append(PageBreak())

    build_document(doc=doc, elements=elements, version=ver)

    # ----------------------------------------------------------------------


def generate_signed_invoice(fname, invoice_p, exporter_p, importer_p, document_signature_date, tags=True):

    """
    #TODO: Rewrite restructured text

    :param fname: Filename to write to
    :param invoice_fname: Invoice file name to read from
    :param exporter_contact: Exporter contact name
    :param exporter_name: Exporter company name
    :param exporter_email: Exporter contact Email
    :param importer_contact: Importer contact name
    :param importer_name: Importer company name
    :param importer_email: Importer contact Email
    :param date: Today's date
    :param tags: Whether signer page should include Hellosign tags

    :type fname: str
    :type invoice_fname: str
    :type exporter_contact: str
    :type exporter_name: str
    :type exporter_email: str
    :type importer_contact: str
    :type importer_name: str
    :type importer_email: str
    :type date: str
    :type tags: str
    :return:
    :raises: None


    """
    image_flowable = read_pdf_convert_rescale(fname=invoice_p["invoice_fname"])


    doc = SimpleDocTemplate(fname, pagesize=letter) # define doc type
    elements = []

    paragraphs, ver = file_to_Text("PO_declaration.txt") # Read paragraphs without and jinja2 replacements

    elements.append(image_flowable)  # append image file
    elements.append(PageBreak())

    for x in paragraphs: # Render all paragraphs
        elements.append(Paragraph(x,PS("Justify")))

    elements.append(PageBreak())

    # generate a signer page

    signers = []
    signers.append({'name': exporter_p["contact"], 'company': exporter_p["name"], 'email_address': exporter_p["email"]})
    signers.append({'name': importer_p["contact"], 'company': importer_p["name"], 'email_address': importer_p["email"]})

    SignerPage = SignaturePage(signers=signers, tags=tags, date=document_signature_date)
    elements.append(SignerPage)
    elements.append(PageBreak())

    build_document(doc=doc, elements=elements, version=ver)


def generate_assignment_notice(fname, importer_p, transigo_p, document_signature_date, closing_date,
                               invoice_p, tags=True):

    """
    #TODO rewrite restructured text
    Generate the assignment notice given by Transigo to the importer


    """

    centered = PS(name='centered',
                  fontSize=12,
                  leading=16,
                  alignment=1,
                  spaceAfter=10)

    paragraphs, ver = file_to_Text("assignment_notice.txt",
                                   closing_date= closing_date, importer_contact=importer_p["contact"],
                                   importer_name=importer_p["name"],
                                   importer_address=importer_p["address"], invoice_number=invoice_p["invoice_num"],
                                   transigo_contact=transigo_p["contact"], transigo_email=transigo_p["contact_email"],
                                   transigo_phone=transigo_p["phone"], transigo_address=transigo_p["snail_mail"])

    doc = SimpleDocTemplate(fname, pagesize=letter)

    elements= []

    for x in paragraphs:
            elements.append(Paragraph(x,PS("Justify")))

    elements.append(PageBreak())

    signers= [{'name': transigo_p["contact"], 'company': transigo_p["name"], 'email_address': transigo_p["contact_email"]}]
    elements.append(SignaturePage(signers=signers, date=document_signature_date, tags=tags))

    elements.append(PageBreak())

    elements.append(Paragraph('<b><u>Appendix A</u></b>',centered))
    elements.append(Paragraph('<b><u>Transigo Inc. Wire Details</u></b>',centered))

    payment_information(elements)


    # write the document to disk

    build_document(doc=doc, elements=elements, version=ver)

if __name__ == "__main__":
    with open("deal1.yaml", 'r') as stream:
        try:
            params=yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    yaml_exporter_p = params['exporter']
    yaml_importer_p = params['importer']
    yaml_deal_p = params["deal"]
    yaml_po_pi_p = params["po_pi"] #Purchase order/ proforma invoice details
    yaml_invoice_p = params["invoice"]
    yaml_dates_p= params["dates"]

    with open("transigo.yaml", 'r') as stream:
        try:
            params=yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    yaml_transigo_p= params['transigo']

    generate_importer_acceptance_agreement(fname="results/ImporterAcceptance.pdf", exporter_p=yaml_exporter_p, importer_p=yaml_importer_p, transigo_p=yaml_transigo_p,
                                           deal_p=yaml_deal_p,po_pi_p=yaml_po_pi_p,
                                           document_signature_date=yaml_dates_p["importer_acceptance_date"])

    generate_exporter_schedA(fname="results/SchedA.pdf", importer_p=yaml_importer_p, exporter_p=yaml_exporter_p, transigo_p=yaml_transigo_p,
                             deal_p=yaml_deal_p,po_pi_p=yaml_po_pi_p,
                             document_signature_date=yaml_dates_p["exporter_schedA_date"])

    generate_exporter_appendixC(fname="results/AppendixC.pdf", importer_p=yaml_importer_p,
                                exporter_p=yaml_exporter_p, transigo_p=yaml_transigo_p, deal_p=yaml_deal_p, invoice_p=yaml_invoice_p,
                                document_signature_date=yaml_dates_p["closing_date_AppendixC_date"])

    generate_signed_invoice(fname= "results/signedInvoice.pdf",invoice_p=yaml_invoice_p, exporter_p=yaml_exporter_p, importer_p=yaml_importer_p,
                       document_signature_date= yaml_dates_p["invoice_signing_date"])

    generate_assignment_notice(fname="results/AssignmentNoticeZoola.pdf", importer_p=yaml_importer_p,
                               transigo_p=yaml_transigo_p, invoice_p=yaml_invoice_p, document_signature_date=yaml_dates_p["assignment_date"],
                               closing_date=yaml_dates_p["closing_date_AppendixC_date"])

    print("importer_acceptance: %s" % (hash_reportlab_PDF_file("results/ImporterAcceptance.pdf")))
    print("SchedA: %s" % (hash_reportlab_PDF_file("results/SchedA.pdf")))
    print("AppendixC: %s" % (hash_reportlab_PDF_file("results/AppendixC.pdf")))
    print("signedInvoice: %s" % (hash_reportlab_PDF_file("results/signedInvoice.pdf")))
    print("Assignment Notice: %s" % (hash_reportlab_PDF_file("results/AssignmentNoticeZoola.pdf")))



pass