import unittest
from utils import hash_reportlab_PDF_file
from doctools import generate_exporter_appendixC, generate_assignment_notice, generate_exporter_schedA
from doctools import generate_importer_acceptance_agreement, generate_signed_invoice
import yaml
import os

def read_yaml_params(deal_fname, transigo_fname="../transigo.yaml"):
    with open(deal_fname, 'r') as stream:
        try:
            params=yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    yaml_exporter_p = params['exporter']
    yaml_importer_p = params['importer']
    yaml_deal_p = params["deal"]
    yaml_po_pi_p = params["po_pi"] #Purchase order/ proforma invoice details
    yaml_invoice_p = params["invoice"]
    yaml_dates_p= params["dates"]

    with open(transigo_fname, 'r') as stream:
        try:
            params=yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    yaml_transigo_p= params['transigo']

    return yaml_exporter_p, yaml_importer_p, yaml_deal_p, yaml_po_pi_p, yaml_invoice_p, yaml_dates_p, yaml_transigo_p

class VerifyDocumentHash(unittest.TestCase):
    def setUp(self):
        self.yaml_exporter_p, self.yaml_importer_p, self.yaml_deal_p, self.yaml_po_pi_p, self.yaml_invoice_p, self.yaml_dates_p, self.yaml_transigo_p =\
        read_yaml_params("deal1.yaml", "transigo.yaml")

    def test_generate_exporter_appendixC(self):
        #self.setup()
        generate_exporter_appendixC(fname="results/AppendixC_test.pdf", importer_p=self.yaml_importer_p,
                                     exporter_p=self.yaml_exporter_p, transigo_p=self.yaml_transigo_p, deal_p=self.yaml_deal_p,
                                     invoice_p=self.yaml_invoice_p,
                                     document_signature_date=self.yaml_dates_p["closing_date_AppendixC_date"])
        digest = hash_reportlab_PDF_file("results/AppendixC_test.pdf")

        self.assertTrue(digest == "531ab4517663a18a165845d46766e3e6975637e488a9eb7902d047505ee65a5a")

    def test_generate_assignment_notice(self):
        #self.setup()
        generate_assignment_notice(fname="results/AssignmentNoticeZoola_test.pdf", importer_p=self.yaml_importer_p,
                                   transigo_p=self.yaml_transigo_p, invoice_p=self.yaml_invoice_p,
                                   document_signature_date=self.yaml_dates_p["assignment_date"],
                                   closing_date=self.yaml_dates_p["closing_date_AppendixC_date"])

        digest = hash_reportlab_PDF_file("results/AssignmentNoticeZoola_test.pdf")
        self.assertTrue(digest == "a7d1dbec60adb04ed4dd6ce8a2052a8a420b178afdc9385f037da88f7d5ab7dd")

    def test_generate_exporter_schedA(self):
        #self.setup()
        generate_exporter_schedA(fname="results/SchedA_test.pdf", importer_p=self.yaml_importer_p, exporter_p=self.yaml_exporter_p,
                                 transigo_p=self.yaml_transigo_p,
                                 deal_p=self.yaml_deal_p, po_pi_p=self.yaml_po_pi_p,
                                 document_signature_date=self.yaml_dates_p["exporter_schedA_date"])
        digest = hash_reportlab_PDF_file("results/SchedA_test.pdf")
        self.assertTrue(digest == "4184bfd6e3f32e420ade9fe3bb5cea680d0af67e02c694eb3fd34b290e0546bf")

    def test_generate_importer_acceptance_agreement(self):
        #self.setup()
        generate_importer_acceptance_agreement(fname="results/ImporterAcceptance_test.pdf", exporter_p=self.yaml_exporter_p,
                                               importer_p=self.yaml_importer_p, transigo_p=self.yaml_transigo_p,
                                               deal_p=self.yaml_deal_p, po_pi_p=self.yaml_po_pi_p,
                                               document_signature_date=self.yaml_dates_p["importer_acceptance_date"])
        digest = hash_reportlab_PDF_file("results/ImporterAcceptance_test.pdf")
        self.assertTrue(digest == "85659ca648090b29a268337798f34015b2f55ba7689ee780be73ef3da0eb63c2")

    def test_generate_signed_invoice(self):
        #self.setup()
        generate_signed_invoice(fname="results/signedInvoice_test.pdf", invoice_p=self.yaml_invoice_p, exporter_p=self.yaml_exporter_p,
                                importer_p=self.yaml_importer_p, document_signature_date=self.yaml_dates_p["invoice_signing_date"])
        digest = hash_reportlab_PDF_file("results/signedInvoice_test.pdf")
        self.assertTrue(digest == "8e56efdfe3f2de22751213c6b99385dbbc3eba49bdff3ae4da66526f211aea9a")

    def tearDown(self):
        os.system("rm -rf results/AppendixC_test.pdf")
        os.system("rm -rf results/AssignmentNoticeZoola_test.pdf")
        os.system("rm -rf results/SchedA_test.pdf")
        os.system("rm -rf results/ImporterAcceptance_test.pdf")
        os.system("rm -rf results/signedInvoice_test.pdf")

