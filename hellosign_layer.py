
#Author: Nir Tal
#Orignally created: 12/08/2018

import ast
import json
import os
import argparse
import log_layer
import common_stuff
from hellosign_sdk import HSClient
import time

RELATIVE_PATH_TO_API_KEY   = "/Configuration"

ABSOLUTE_PATH_TO_API_KEY   = common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + RELATIVE_PATH_TO_API_KEY

class HS_layer:
    def __init__(self,api_key,client_id):
        self.api_key = api_key
        self.client_id = client_id
        self.client = client = HSClient(api_key=self.api_key)

    def GenerateEmbeddedRequest(self, signers, file_path, title, subject, message, files, test_mode=True,
                                generate_test_file=False):

        HS_signers = []

        for ind,x in enumerate(signers):
            HS_signers.append({"name": x["name"], "email_address": x["email_address"], "order": "%d" % (ind+1)})

        GenerateSignerPage(signers)
        files.append("sig.pdf")
        ret = self.client.send_signature_request_embedded(
            client_id=self.client_id,
            test_mode=test_mode,  # TODO: after we pay for appropriate plan, this should become False!
            title=title,
            subject=subject,
            message=message,
            signers=HS_signers,
            # files=[args.abs_path_to_appendix, args.abs_path_to_file]
            files=files,
            use_text_tags=True, hide_text_tags=True,

        )

        signatures = []
        urls = []

        for x in signers:
            signatures.append(ret.find_signature(signer_email_address=x['email_address']))

        for (ind, x) in enumerate(signatures):
            print(x.json_data['signature_id'])
            embedded_obj = self.client.get_embedded_object(signature_id=x.json_data['signature_id'])
            print(embedded_obj)
            urls.append(embedded_obj.json_data["sign_url"])
            if generate_test_file:
                GenerateTestFile(url="%s" % embedded_obj.json_data["sign_url"], fname="hellosign%d.html" % (ind))

        return ret.json_data['signature_request_id'], signatures, urls


    def CheckSignatureStatus(self, sig_req):
        X = self.client.get_signature_request(sig_req)
        return X.json_data["is_complete"],X.json_data["is_declined"], X.json_data["has_error"], X


    def GetSignedDocument(self,request_id,filename):
        self.client.get_signature_request_file(
            signature_request_id=request_id,
            filename=filename,
            file_type='pdf'
        )

    def GetIncompleteSignatureList(self):
        srl = self.client.get_signature_request_list()
        sigs =[]
        for x in srl:
            if (x.json_data['is_complete'] == False):
                sig_id = x.json_data['signature_request_id']
                sigs.append(sig_id)

        return sigs

    def CancelSignatureRequest(self,sig_id):
        self.client.cancel_signature_request(sig_id)

    def CancelAllIncompleteSignatureRequests(self):
        sig_list = self.GetIncompleteSignatureList()

        for x in sig_list:
            self.CancelSignatureRequest(x)
            print(x)



def GenerateTestFile (url,app_key="8c26f4299e5304f088e971cffe0b7ce6",fname="hellosign.html"):

    fid=open(fname,"w")

    fid.write("<!doctype html>\n")
    fid.write("<html>\n")
    fid.write("<head>\n")
    fid.write("<script src = \"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"> </script>\n")

    fid.write("<script type = \"text/javascript\" ")
    fid.write("src = \"https://s3.amazonaws.com/cdn.hellosign.com/public/js/hellosign-embedded.LATEST.min.js\"></script>\n")

    fid.write("<script type = \"text/javascript\">\n")
    fid.write("    $(document).ready(function(){\n")
    fid.write("HelloSign.init(\"%s\");\n" % (app_key))

    fid.write("HelloSign.open({\n")
    fid.write("url: \"%s\",\n" % (url))
    fid.write("allowCancel: true,\n")
    fid.write("skipDomainVerification: true,\n")
    fid.write("messageListener: function(eventData) {\n")
    fid.write("console.log(eventData);\n")
    fid.write("// do something\n")
    fid.write("}\n")
    fid.write("});\n")
    fid.write("});\n")
    fid.write("</script>\n")
    fid.write("</head>\n")
    fid.write("<body>\n")
    fid.write("</body>\n")
    fid.write("</html>\n")
    fid.close()



def ReadApiKeyFromFile () :

    API_CONFIG_FILE_NAME = "Hellosign_Config"

    wd_upon_call = os.getcwd()

    os.chdir(ABSOLUTE_PATH_TO_API_KEY)

    #TODO: this assumes that the api-key is the only thing in the file (and is saved as-is). we should probably create a smarter format, to allow adding further parameters.

    #open, read and close the file
    
    print ('trying to read file %s' % ABSOLUTE_PATH_TO_API_KEY + '/' + API_CONFIG_FILE_NAME)
    
    with open(API_CONFIG_FILE_NAME , 'r') as myfile :
        api_key = myfile.read().rstrip()


    #go back to original directory, to avoid side-effects
    os.chdir(wd_upon_call)

    return (api_key)




#main flow

if (__name__ == "__main__"):

    A = {'name': 'Jason Robinson', 'company': 'Transigo', 'email_address': 'jason.robinson@transigo.io'}
    B = {'name': 'Bill Gates', 'company': 'Microsoft', 'email_address': 'bill@microsoft.com'}
    C = {'name': 'Jeff Bezos', 'company': 'Amazon', 'email_address': 'jeff@amazon.com'}
    D = {'name': 'Marissa Mayer', 'company': 'Yahoo', 'email_address': 'marissa@yahoo.com'}

    signers = [A,B]

    try:

        API_KEY = ReadApiKeyFromFile ()

    except Exception as ex :

        log_layer.WriteToLog ("Failed to reap API key!" , "error")
        log_layer.WriteToLog (ex , "error")
        exit ()

    client_id_str = "8c26f4299e5304f088e971cffe0b7ce6"


    HS = HS_layer(api_key=API_KEY, client_id = client_id_str)

    print("Canceling all incomplete signature requests")

    HS.CancelAllIncompleteSignatureRequests()

    sig_id, sigs, urls = HS.GenerateEmbeddedRequest(signers=signers,file_path='PDF/GettysburgAddress.pdf',
                            title = 'Title of this message', message='Test',
                            subject='Test Command', files=["/home/nt/signer/pdfphysicalsigner/PDF/GettysburgAddress.pdf"],
                            generate_test_file = True

                            )
    is_done, is_declined, has_error, _ =HS.CheckSignatureStatus(sig_req = sig_id)

    print("is done: %s is_declined: %s has_error: %s" % (is_done, is_declined, has_error))


    print("Please Sign")

    print("Polling")

    while True:
        is_done, is_declined, has_error, _ = HS.CheckSignatureStatus(sig_req=sig_id)
        print("is done: %s is_declined %s has_error %s" % (is_done, is_declined, has_error))
        time.sleep(2)
        if is_done:
            break

    HS.GetSignedDocument(request_id=sig_id, filename="signed_document.pdf")



    print("is done: %s is declined: %s has error: %s" % (is_done, is_declined, has_error))


    pass

    exit(0)

    parser=argparse.ArgumentParser(
        description='''This utility allows the caller to send invetations to physically sign a pdf file by two parties, using the Hellosign API. \
         Note that the signers are *not* required to have a user in Hellosign (which makes it easier for them, but also lacks safety mechanisms). \
         Also, note that *all* argument fields are required!
         ''',
        epilog="""All rights reserved to Trasigo Ltd 2018.\n Any possible errors are direct fault of Jason Robinson and he takes full responsibility for them.\nUse with caution!""")
    parser.add_argument('--test_mode', type=str, default=True, help='True (if we didnt pay for full API) or False otherwise!')
    parser.add_argument('--title'    , type=str, help='The document title')
    parser.add_argument('--subject'  , type=str, help='The *subject* of the invitation email the signers will receive')
    parser.add_argument('--message'  , type=str, help='the *content* of the invitation email the signers will recieve')
    parser.add_argument('--signer0', type=str,help='string representing dictionary for the first signer, e.g \"{ \'email_address\': \'jason@transigo.io\', \'name\': \'Jack\' }\" ') #TODO: improve this to allow a list of any length!
    parser.add_argument('--signer1', type=str,help='dictionary for the second signer, e.g \"{ \'email_address\': \'jason@transigo.io\', \'name\': \'Jack\' }\" ')
    parser.add_argument('--abs_path_to_file'  , type=str, help='absoulte path to the PDF file that should be signed')
    parser.add_argument('--abs_path_to_appendix', type=str, help='absoulte path to the apendix that must be added') #TODO: consider making this an optional field!

    args=parser.parse_args()

    print (args.signer0)
    print(args.signer1)




    #use a temp variable to handle the weird way this langugae bahaves
    # we were unable to send the dictionary directly as argument and to use a variable in send_signature_request method without assigning inside it!
    #TODO: find a cleaner way to get all of this done
    my_signers = []
    my_signers.append(ast.literal_eval(args.signer0))
    my_signers.append(ast.literal_eval(args.signer1))

    print (my_signers)
    #send_signature_request
    #

    client = HSClient(api_key=API_KEY)

    #delete all previous requests

    srl = client.get_signature_request_list()

    for x in srl:
        if (x.json_data['is_complete'] == False):
            sig_id = x.json_data['signature_request_id']
            print("Canceling %s" % (sig_id) )
            client.cancel_signature_request(sig_id)

    EmbeddedSigning = True
    if (EmbeddedSigning == True):

        ret = client.send_signature_request_embedded(
            client_id=client_id_str,
            test_mode=args.test_mode, #TODO: after we pay for appropriate plan, this should become False!
            title=args.title,
            subject=args.subject,
            message=args.message,
            signers=my_signers,
            #files=[args.abs_path_to_appendix, args.abs_path_to_file]
            files=[args.abs_path_to_file, "sig.pdf" ],
            use_text_tags=True, hide_text_tags=True,

        )


    else:

        ret = client.send_signature_request(
            test_mode=args.test_mode,  # TODO: after we pay for appropriate plan, this should become False!
            title=args.title,
            subject=args.subject,
            message=args.message,

            signers=my_signers,

            # files=[common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + '/GettysburgAddress.pdf', common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + '/Appendix.pdf']
            files=[args.abs_path_to_appendix, args.abs_path_to_file],
        )

    signatures = []
    for x in my_signers:
        signatures.append(ret.find_signature(signer_email_address=x['email_address']))

    client2 = HSClient(api_key=API_KEY)

    if (True):
        # not sure about this section


    # open all signer embedded_obj

        for (ind,x) in enumerate(signatures):
            print (x.json_data['signature_id'])
            embedded_obj = client2.get_embedded_object(signature_id=x.json_data['signature_id'])
            print(embedded_obj)
            GenerateTestFile(url = "%s" % embedded_obj.json_data["sign_url"],fname = "hellosign%d.html" % (ind))
            #print ("%s?client_id=%s" %(embedded_obj,client_id_str))
        pass


 #   print(embedded_obj)
