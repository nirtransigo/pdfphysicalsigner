from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Spacer
import babel.numbers as bn
from  reportlab.lib.styles import ParagraphStyle as PS
from  reportlab.platypus import PageBreak, Flowable
from  reportlab.platypus.paragraph import Paragraph
import reportlab.platypus as pltpus
import jinja2
from wand.image import Image
#from wand.display import display
from wand.image import Color
import hashlib as hl
import re

version = "" # This disgusting solution happens so we can pass arguments to the PageNumCanvas

#from reportlab.lib.styles import getSampleStyleSheet

# class WrongTypeException(BaseException):
#     def __init__(self):
#         pass



class UnableToReadImageFile(BaseException):
    def __init__(self):
        pass

class UnableToSaveImageFile(BaseException):
    def __init(self):
        pass

class FormatCurrency(BaseException):
    """
    Class to format currency (i.e. $12,345.22)
    """

    def __init__ (self,currency, locale):
        """

        :param currency: Currency to format to (i.e. "USD")
        :param locale: Locale to use (i.e. "en-US")

        :type currency: str
        :type locale: str

        :return: None

        """
        self.currency = currency
        self.locale = locale

    def PrintM(self,x):
        return bn.format_currency(x,currency=self.currency,locale=self.locale)

#deprecated

# def addPageNumber(canvas, doc):
#     """
#     Add the page number based on:
#         http://www.blog.pythonlibrary.org/2013/08/12/reportlab-how-to-add-page-numbers/
#     """
#     page_num = canvas.getPageNumber()
#     text = "Page #%s" % (page_num)
#     canvas.drawRightString(7.8 * inch, 0.78 * inch, text)


def hash_reportlab_PDF_file(fname):
    """
    Perform hash on file using sha3-256 for reportlab generated PDF files.
    This functions reads the entire file content into memory
    and performs the hash digest after replacing 3 annoying phrases generated
    by reportlab upon build.


    :param fname: File name

    :type fname: str

    :return: Hash digest
    :rtype: str

    """

    hasher = hl.sha3_256()

    try:
        with open(fname,"rb") as fid:
            buf = fid.read()
            fid.close()
    except:
        raise FileNotFoundError()

    buf=re.sub(b'CreationDate \(D:\d{14}\+\d\d\'\d\d\'\)',b'',buf)
    buf = re.sub(b'ModDate \(D:\d{14}\+\d\d\'\d\d\'\)', b'', buf)
    buf=re.sub(b"\[\<[0-9a-f]{32}\>\<[0-9a-f]{32}\>\]",b'',buf)

    hasher.update(buf)
    digest = hasher.hexdigest()

    return digest

def read_pdf_convert_rescale(fname,x_size = 6 * inch):
    """

    Read a PDF diagram and scale it proportionally

    :param fname: Filename to read
    :param x_size: X axis size

    :return: Image flowable

    :type fname: str
    :type x_size: float
    :rtype: element

    :raises UnableToReadImageFile: Unable to either read PDF file or temporary file
    :raises UnableToSaveImageFile: Unable to save temporary image file
    """

    try:
        img= Image(filename=fname, resolution=300)
    except:
        raise UnableToReadImageFile()

    img.background_color = Color("white")
    img.alpha_channel = 'remove' # due to https://stackoverflow.com/questions/46491452/python-wand-convert-pdf-to-black-image

    try:
        img.save(filename='image.png') # Save temporary png file
    except:
        raise UnableToSaveImageFile()

    try:
        I = pltpus.Image('image.png') # read the image
    except:
        raise UnableToReadImageFile()

    I.drawHeight = x_size * I.drawHeight / I.drawWidth # scale it proportiohnally
    I.drawWidth = x_size

    return I



def build_document(doc, elements, version):
    """
    Build the document using the elements and the version. **DO NOT CALL doc.build outside this function for risk of
    stale version (due to static member)**.

    :param doc: document class
    :param elements: Elements list for page building
    :param version: Version string

    :type doc: document
    :type elements: list of element
    :type version: str

    :raises: None
    :return: None
    """

    PageNumCanvas.version = version  # store version text in static member
    doc.build(elements, canvasmaker=PageNumCanvas)


def wrap_text_and_compute_height(data,table_width=3*inch, spacer=0.2*inch):
    """
    Computes the height list for every row in a table. Computes the maximal height of all columns for every row
    and adds a spacer height.

    :param data: A list of lists with paragraphs that go into table
    :param table_width: Table width
    :param spacer: Spacer height to add to each row

    :type data: list of list of Paragraph
    :type table_width: float
    :type spacer: float

    :return: list of row heights
    :rtype: list of float

    """

    height = []
    for row in range(0,len(data)):
        w=0
        for col in range(0,len(data[0])):
            data[row][col].wrap(table_width,1000000)
            if data[row][col].height > w:
                w=data[row][col].height
        height.append(w+spacer)
    return height


    # ----------------------------------------------------------------------


def file_to_Text(fname,fieldbackgroundcolor="#e0e0e0",**kwargs):
    """
    Formats a text file into a set of paragraphs. Converts any \n to <br/> tag.
    Cuts paragraphs at the @@ marker.
    If file starts with a version indicator and ##, returns the version-indicator. Never put a \n after the ##.

    :param fname: File name to read
    :param fieldbackgroundcolor: Background color for the various fields (format #rrggbb hex coding)
    :param kwargs: Arguments for field replacements


    :type fname: str
    :type fieldbackgroundcolor: str
    :type kwargs: dict

    :return: list of formatted strings
    :return: version indicator

    :rtype: list of str
    :rtype: str

    :raises FileNotFoundError: Couldn't open file for read
    """

    try:
        fid = open(fname,"r")
    except:
        raise FileNotFoundError()
    text = fid.read()

    fid.close()
    z=text.find("##") #find the version marker

    if (z==-1): #version marker not found
        ver=""
    else:
        ver=text[:z]
        text=text[z+2:]

    ind =0
    paragraphs=[]
    start_ind = 0

    while (ind<len(text)):
        if text[ind]=="\n": # replace \n with <br/>
            text = text[:ind] + "<br/>" + text[ind+1:]
            ind += 5
        elif text[ind] =="@":
            if text[ind+1] == "@": #Check for @@ section splits
                paragraphs.append(text[start_ind:ind]) #generate a list of paragraphs split by the @@ marker
                start_ind=ind+2
                ind+=2
        else:
            ind += 1
    # append last paragraph
    paragraphs.append(text[start_ind:])
    pass

    for x in kwargs.keys(): # add background color to fields
        kwargs[x] = "<font backColor=%s>" % (fieldbackgroundcolor) + kwargs[x] + "</font>"

    rendered = []
    for x in paragraphs:
        jTemplate = jinja2.Template(x) # Generate template for each paragraph
        rendered.append(jTemplate.render(**kwargs)) #Render it with the jinja2 module and the given kwargs parameters

    version = ver
    return rendered, ver # return the rendered paragraphs and the version

    # ----------------------------------------------------------------------

class SignaturePage(Flowable):

    """
    Generate a reportlab flowable signature page

    based on http://www.blog.pythonlibrary.org/2014/03/10/reportlab-how-to-create-custom-flowables/


    """
    def __init__(self, signers, date, tags = True, coords=(10,500,290,500,10, 300, 290, 300)):
        """

        :param signers: list of dictionary with the following fields: "name", "company", "email_address"
        :param date: Today's date
        :param tags: Add Hellosign tags?
        :param coords: Coordinates for signatures

        :type signers: list of dict
        :type date: str
        :type tags: bool
        :type coords: tuple

        :raises SignersNumberError: Number of signers too small or too large

        """

        Flowable.__init__(self)
        if (len(signers) <= 0) or (len(signers) > 4):
            raise SignersNumberError()
        self.signers = signers
        self.coords = coords
        self.tags = tags
        self.date = date
        self.width =400
        self.height = 600

    def __repr__(self):
        return "Signer page"


    def draw(self):
        """
        draw the page

        """
        self.canv.drawString(10,600,"IN WITNESS WHEREOF, the undersigned Parties have caused this Agreement to be executed")
        self.canv.drawString(10,580, "of this %s" % (self.date))

        for ind in range(0, len(self.signers)):
            X = self.coords[2 * ind]
            Y = self.coords[2 * ind + 1]
            self.canv.line(X, Y + 20, X + 180, Y + 20)
            self.canv.drawString(X, Y, "%s" % (self.signers[ind]["name"]))
            self.canv.drawString(X, Y - 20, "%s" % (self.signers[ind]["company"]))
            self.canv.drawString(X, Y - 40, "%s" % (self.signers[ind]["email_address"]))
            self.canv.drawString(X, Y - 60, "Date: ")
            if self.tags:
                self.canv.drawString(X, Y + 40, "[sig|req|signer%d]" % (ind + 1))
                self.canv.drawString(X + 40, Y - 60, "[date|req|signer%d]" % (ind + 1))
class PageNumCanvas(canvas.Canvas):
    """
    This function allows the addition of page numbers, a total page count and a revision name for each document
    on each page

    http://code.activestate.com/recipes/546511-page-x-of-y-with-reportlab/
    http://code.activestate.com/recipes/576832/
    """
    version = ""
    # ----------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """Constructor

        """

        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []


    # ----------------------------------------------------------------------
    def showPage(self):
        """
        On a page break, add information to the list
        """
        self.pages.append(dict(self.__dict__))
        self._startPage()

    # ----------------------------------------------------------------------
    def save(self):
        """
        Add the page number to each page (page x of y)
        """
        page_count = len(self.pages)

        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            self.draw_version()
            canvas.Canvas.showPage(self)

        canvas.Canvas.save(self)

    # ----------------------------------------------------------------------
    def draw_page_number(self, page_count):
        """
        Add the page number and page count
        """
        page = "Page %s of %s" % (self._pageNumber, page_count)
        self.setFont("Helvetica", 9)
        self.drawRightString(7.6 * inch, 0.5 * inch, page)

    # ----------------------------------------------------------------------

    def draw_version(self):
        """
        Add the revision name from a static variable called version
        """
        self.setFont("Helvetica", 6)
        self.setFillColorRGB(0.3,0.3,0.3)
        self.drawRightString(7 * inch, 10.5 * inch, "[" + self.version + "]")

#deprecated
# def generate_signer_page():
#
#
#     if (len(signers)<=0) or (len(signers)>4):
#         raise SignersNumberError
#
#
#     c = canvas.Canvas("sig.pdf")
#
#     #c.drawString(coords[0], coords[1]+20, "-"*40)
#
#
#     for ind in range(0,len(signers)):
#         X = coords[2*ind]
#         Y = coords[2*ind+1]
#         c.drawString(X,Y+40,"[sig|req|signer%d]" %(ind+1))
#         c.line(X,Y+20,X+180,Y+20)
#         c.drawString(X, Y, "%s" % (signers[ind]["name"]))
#         c.drawString(X, Y-20, "%s" %(signers[ind]["company"]))
#         c.drawString(X, Y-40, "%s" %(signers[ind]["email_address"]))
#         c.drawString(X, Y-60, "Date: ")
#         c.drawString(X+40, Y-60, "[date|req|signer%d]" %(ind+1))
#
#     #c.drawString(200,200,"[text-merge|req|sender|blibla|8c26f4299e5304f088e971cffe0b7ce6]")
#
#     c.save()

    #----------------------------------------------------------------------

